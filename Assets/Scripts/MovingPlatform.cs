﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovingPlatform : MonoBehaviour
{
	private Vector3 start;
	public Vector3 end;

	public float travelTime;
	private float currentTime;

	private bool forward = true;
	private HashSet<Transform> colliders = new HashSet<Transform>(); 

	void Start()
	{
		start = this.transform.position;
		end += this.transform.position;
	}
	
	void Update()
	{
		float dt = Time.deltaTime;
		
		if (forward) {
			currentTime += dt;
			if (currentTime >= travelTime) {
				forward = false;
			}
		} else {
			currentTime -= dt;
			if (currentTime <= 0) {
				forward = true;
			}
		}
		
		Vector3 newPosition = Vector3.Lerp (start, end, currentTime/travelTime);
		Vector3 translation = newPosition - transform.position;
		transform.position = newPosition;
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		coll.transform.SetParent (transform);
	}
	
	void OnTriggerExit2D(Collider2D coll)
	{
	    coll.transform.SetParent (null);
	}
}